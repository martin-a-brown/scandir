# -*- coding: utf-8 -*-
import os

import glob
from setuptools import setup
from msamwa import VERSION


with open(os.path.join(os.path.dirname(__file__), 'README.md')) as r_file:
    readme = r_file.read()


setup(
    name='msamwa',
    version=VERSION,
    license='MIT',
    author='Marcin',
    author_email='devnull@bitbucket.org',
    url="https://gitlab.com/marcina/scandir",
    description='MSA MWA directory scanning analyzer',
    long_description=readme,
    packages=['msamwa',],
    install_requires=['magic'],
    include_package_data=True,
    entry_points={
        'console_scripts': ['malware-analyzer = msamwa.driver:main', ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Documentation',
    ],
)
