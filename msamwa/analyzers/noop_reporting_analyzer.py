#! /usr/bin/python
# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import os
import sys


def noop_reporting_analyzer(fname, mimetype, extras):
    print("Yes, %s is a %s (extras=%r)." % (fname, mimetype, extras))
    sys.exit(os.EX_OK)


def main(*args):
    noop_reporting_analyzer(*args)


if __name__ == '__main__':
    if sys.argv >= 3:
        fname, mimetype = sys.argv[1:3]
        main(fname, mimetype, sys.argv[3:])
    else:
        argv0 = os.path.basename(sys.argv[0])
        usage = "Usage: %s <filename> <mimetype> [...]"
        sys.exit(usage % (argv0,))

# -- end of file
