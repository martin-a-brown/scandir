#! /usr/bin/python
#
# -- tiny sample script to pick a random analyzer from the directory
#    in which it sits

import os
import sys
import random


def isexecutable(f):
    '''True if argument is executable'''
    return os.path.isfile(f) and os.access(f, os.X_OK)


def absolutize(relnames, dirname):
    return [os.path.join(dirname, x) for x in relnames]


def find_executable_siblings(dirname):
    siblings = absolutize(os.listdir(dirname), dirname)
    return [x for x in siblings if isexecutable(x)]


def get_random_sibling(dirname):
    siblings = find_executable_siblings(dirname)
    return random.choice(siblings)


def exec_random_sibling(dirname, args):
    sibling = get_random_sibling(dirname)
    args.insert(0, sibling)
    os.execv(sibling, args)


if __name__ == '__main__':
    dirname = os.path.dirname(os.path.abspath(__file__))
    exec_random_sibling(dirname, sys.argv[1:])

# -- end of file
