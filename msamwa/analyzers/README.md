Analyzers live in this directory.

Each analyzer should expect to be run with at least two arguments:

  <analyzer> <fname> <mimetype> [...]

These are the meanings:

  <fname>: a path to the subject under test for the analyzer
  <mimetype>: is the detected MIME type (using magic)

Any arguments after the first two are free to be individually specified by each
analyzer to allow for custom behaviour.  (But how would they be passed?)

The analyzer can perform any operation it would like on the file.  The MIME
type is merely ancillary information.

The analyzer must convey success or failure with its exit code.  Each analyzer
may define different meanings for exit codes other than 0.  As usual on Unix
systems, an exit code of 0 means that the analyzer succeeded (whatever that
may mean).
