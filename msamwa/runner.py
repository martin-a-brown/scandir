#! /usr/bin/python
# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import os
import subprocess


def execute_analyzer(cmd, env=os.environ):
    '''call child command, return (retcode, stdout, stderr)

    this function is a wrapper around Popen, so that the caller
    only needs to provide a command (list form)

    the subprocess.Popen().communicate() function will buffer
    all STDOUT and STDERR from the child command

    after the command terminates, this function collects the
    return code, the STDOUT and STDERR and returns as a 3-tuple
    '''
    proc = subprocess.Popen(cmd,
                            stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            env=env,
                            shell=False,
                            close_fds=True,
                            preexec_fn=os.setsid)
    stdout, stderr = proc.communicate()
    return proc.returncode, stdout, stderr


if __name__ == '__main__':
    import sys
    if len(sys.argv) >= 2:
        rc, stdout, stderr = execute_analyzer(sys.argv[1:])
        print('rc=%s\nstdout=%s\nstderr=%s\n' % (rc, stdout, stderr))
    else:
        argv0 = os.path.basename(sys.argv[0])
        usage = "Usage: %s <filename> <mimetype> <analyzer> [<analyzer> ...]"
        sys.exit(usage % (argv0,))
