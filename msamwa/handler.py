#! /usr/bin/python
# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

from tempfile import NamedTemporaryFile as ntf, mkdtemp
from msamwa.runner import execute_analyzer


def run_analyzer(fname, mimetype, analyzer):
    '''shells out to analyzer, returns (rc, stdout, stderr)'''
    return execute_analyzer([analyzer, fname, mimetype])


def collect_analyzer_output(fname, mimetype, analyzers, dirname):
    prefix = 'badapples-' + os.path.basename(fname) + '.'
    record = ntf(prefix=prefix, dir=dirname, delete=False)
    result = list()
    for analyzer in analyzers:
        cmd = [analyzer, fname, mimetype]
        print('Executing command: %r' % (cmd,), file=record)
        print('------------------------------', file=record)
        rc, stdout, stderr = execute_analyzer(cmd)
        print('Exit code: %d' % (rc,), file=record)
        if stderr:
            # -- if any output in STDERR is bad, should check/set
            #    rc to something other than 0
            if rc == 0:
                print('Exit code was 0, but there was STDERR. Setting rc=1.',
                      file=record)
                rc = 1
            print('STDERR\n------', file=record)
            record.write(stderr)
        if stdout:
            print('STDOUT\n------', file=record)
            record.write(stdout)
        print('\n\n', file=record)
        result.append(rc)
    record.close()
    return any(result), result, record.name


def handle(fname, mimetype, analyzers):
    dirname = mkdtemp()
    status, _, tf = collect_analyzer_output(fname, mimetype,
                                                analyzers, dirname)
    record = tf + '.txt'
    os.rename(tf, record)
    return record


def handle_and_print(*args):
    record = handle(*args)
    print(open(record).read())
    print("See %s" % (record,))


if __name__ == '__main__':
    import os
    import sys
    if len(sys.argv) == 1:
        for line in sys.stdin:
            '''#filename mimetype analyzer [analyzer ...]'''
            fname, mimetype, analyzers = line.split(None, 2)
            handle_and_print(fname, mimetype, analyzers)
    elif len(sys.argv) >= 4:
        fname, mimetype = sys.argv[1:3]
        analyzers = sys.argv[3:]
        handle_and_print(fname, mimetype, analyzers)
    else:
        argv0 = os.path.basename(sys.argv[0])
        usage = "Usage: %s <filename> <mimetype> <analyzer> [<analyzer> ...]"
        sys.exit(usage % (argv0,))
