#! /usr/bin/python
# -*- coding: utf8 -*-

# -- several future-proofing imports; mostly, print_function and
#    unicode_literals are the ones that I really want to improve
#    the smoothness of transition to Python3
#
from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import magic

libmagic = magic.open(magic.MAGIC_MIME_TYPE)
libmagic.load()


# - magic installed from Debian repos is not the same version as
#   what is installed via pip. The API below works for system verison,
#   if the pip one is used, AttributeError will be thrown.
#
def classify(fname):
    #
    # magic.MAGIC_MIME_TYPE - only mime type
    # magic.MAGIC_NONE      - all info (codepage,compression)
    mimetype = libmagic.file(fname)
    return mimetype


# -- I (almost) always write this little block as the very first thing
#    in a Python program, because it forces me to make the logic into
#    a function right from the very start (and it isolates the CLI
#    handling), so I don't have to go walking through the code later
#    to try to find all cases of 'sys.argv[2]' or similar.
#
if __name__ == '__main__':
    import sys
    for arg in sys.argv[1:]:
        print(arg, classify(arg))

# -- end of file
