from os import listdir,fork,getpid,waitpid,_exit
from os.path import isfile, join
import subprocess
from shutil import move
import time

data_path = "/home/marcin/data/scandir-test"
completed_path = "/home/marcin/data/scandir-complete"
children = {}

def child_pdf(filename):
    print "new child", getpid()
    _exit(0)

def finish(filename):
    move(filename,completed_path)
    #print "%s finished" % filename

def classify(filename):
    classifier = "/home/marcin/src/marcin/scandir/classifier.py"
    failed_dir = "/home/marcin/data/scandir-failed"
    
    output = subprocess.check_output([classifier,filename])
    print output.to_ascii()
    #    if output == 'application/pdf':
    #        child_pid = fork()
    #    children[child_pid] = filename
    #            if child_pid == 0:
    #                child_pdf(filename)
    #            else:
    #                parent_pid = getpid()
    #                    print "parent: %d, child: %d, file %s" % (parent_pid, child_pid, f)

for f in listdir("/home/marcin/Downloads"):
    filename = join(data_path,f)
    if isfile(filename):
        try:
            ftype = classify(filename)        
        except:
            print "error: moving ",filename
            #move(filename,failed_dir)

while len(children.keys()):
    print children.keys()
    try:
        pid, status = waitpid(0,0)
    except OSError:
        print "Scandir OSError"

    print "%d exited with status %d" % (pid,status)
    if pid in children:
        if status == 0:
            finish(children[pid])
            del children[pid]
    else:
        print "unknown child ", pid
