scandir
=======

A tool to scan a directory full of malware, identify the MIME type of each
file, then call an analyzer (or a bunch of analyzers) on each file, recording
the output for review and identifying any case where the analyzer itself fails.

Assumptions at the outset:
--------------------------
  * You have millions of pieces of malware.
  * You are using (lib)magic to determine the filetype.
  * For each filetype, you may wish to run one or more analyzers.
  * If an analyzer successfully processes a file, you want to make
    note of the results from the analyzer and continue to the next
    analyzer.
  * If all analyzers succeed, remember that the file has been
    analyzed and do not process on subsequent runs.
  * Write new analyzers and add them to the list of analyzers to
    run for a specific filetype.
  * Run lots of analyzers in parallel.

Assumptions about the different functions:
------------------------------------------
  * each analyzer exits 0 or non-zero
  * a single file may be examined by multiple analyzers
  * an analyzer that exits 0 but produces STDERR is considered a non-zero exit
  * any STDOUT (STDERR, too?) from each analyzer is collected into a 
    report for each file and stored in the state directory
  * a file is processed successfully if all analyzers exit 0

Assumptions about human interaction with the software:
------------------------------------------------------
  * a human will write analyzers
  * a human will be able to run and re-run on the same source directory and
    same state directory and the program will pick up where it left off
  * status of individual source files should be reported to STDOUT
  * progress logging to STDERR should be supported

